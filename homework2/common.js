
const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  



function creatUl(arr){
    const root = document.querySelector("#root")
const ul = document.createElement("ul");

arr.forEach(books => {
    try {
        if (!books.author) {
            throw new Error(`Помилка: Об'єкт не має властивості "author"`);
          }else if(!books.name){
            throw new Error(`Помилка: Об'єкт не має властивості "name"`);
          }else if (!books.price){
            throw new Error(`Помилка: Об'єкт не має властивості "price"`);
          }
        const list = document.createElement("li");
    list.textContent = `${books.name} - ${books.author} - ${books.price} `
    ul.append(list)
    } catch (error) {
          console.error(error.message, books) // ще можна прописати ,books і прибрати на 43, 45, 47 JSON
    }
    
});
root.append(ul)
}

creatUl(books)

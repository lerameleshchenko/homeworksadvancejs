                           //fetch

const sendRequest = (url)=>{
    return fetch(url); 
}

const API = 'https://ajax.test-danit.com/api/swapi/'
const ul = document.createElement('ul')
const loader = document.querySelector(".loader");


function renderFilms(){
    sendRequest(`${API}films`)
         .then((response)=> response.json())
         .then((data)=>{
            console.log(data)
             data.forEach(({episodeId,name,openingCrawl,characters}) => {
           const li = document.createElement("li");

           const episode = document.createElement('p');
           episode.innerHTML = `episode ${episodeId}`

           const title = document.createElement("p");
           title.innerHTML = name
           title.style.fontSize = "24px"
           title.style.color = "red"


           const opening = document.createElement("p");
           opening.innerHTML = openingCrawl

           const character = document.createElement("p")
           character.innerText = 'characters:'
           character.style.fontSize = "24px"
           const charactersName = document.createElement("p")


             li.append(title,character,charactersName,episode,opening)
             ul.append(li)
             document.body.append(ul)  
             
             
             characters.forEach((url) => {
                sendRequest(url)
                    .then((response) => response.json())
                    .then((data) => {
                        const characterLi = document.createElement("span");
                        characterLi.textContent = `${data.name}, `;
                        charactersName.append(characterLi);
                    })
                    .finally(()=>{
                        loader.remove() 
                    })

             })
             
             })
         })
         
}

renderFilms()




                //XMLHttpRequest

// const sendRequest = (url)=>{
//     const promise = new Promise(( resolve, reject)=>{
//         const request = new XMLHttpRequest();
//         request.open("GET", url)
//         request.responseType = "json"
//         request.send()
//         request.onload = function(){
//             if (request.status < 300) {
// 				resolve(request.response)
// 			} else {
// 				reject(request.statusText)
// 			}

//         }

//     })

//     return promise
// }

// const API = 'https://ajax.test-danit.com/api/swapi/films'
// const ul = document.createElement('ul')
// const loader = document.querySelector(".loader");

// function renderFilms(){
//     sendRequest(API)
//     .then(data =>{
//         data.forEach(({episodeId,name,openingCrawl,characters}) => {
//             const li = document.createElement("li")

//             const titleName = document.createElement("h4")
//             titleName.innerHTML = name

//             const episode = document.createElement("p")
//             episode.innerHTML =`episode: ${episodeId}`

//             const opening = document.createElement("p")
//             opening.innerHTML = openingCrawl

//             li.append(titleName,episode,opening)
//             ul.append(li)
//             document.body.append(ul)

//             characters.forEach(element => {
//                 sendRequest(element)
//                 .then(data =>{
//                     const characterLi = document.createElement("li");
//                      characterLi.textContent = data.name;
//                        li.append(characterLi);
//                 })
//                 .finally(()=>{
//                    loader.remove() 
//                 })
//             })
//         });
//     })
// }

// renderFilms()
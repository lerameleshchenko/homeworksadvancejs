class Employee{
    constructor(name, age, salary){
     this._name = name;
     this._age = age;
     this._salary = salary;
    }

    get name(){
        return this._name;
    }

    set name (newName){
        return this._name = newName;
    }

    get age(){
        return this._age;
    }

    set age(newAge){
        return this._age = newAge
    }

    get salary(){
        return this._salary;
    }
    set salary(newSalery){
        return this._salary = newSalery;
    }
}

const user = new Employee("Lera", 15, 2000);
console.log("user1",user)
console.log("name",user.name);
console.log("age",user.age);
console.log("salary",user.salary);

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
       this._lang = lang;
    }

    get lang(){
        return this._lang
    }
    set lang(newLang){
        return this._lang = newLang;
    }

    get salary(){
        return this._salary * 3;
    }
    set salary(newSalery){
        return this._salary = newSalery;
    }
}

const userv2 = new Programmer("Dima", 20, 4000, ["JavaScript", "Python"]);

console.log("userv2",userv2)
console.log("name",userv2.name);
console.log("age",userv2.age);
console.log("salary",userv2.salary);
console.log("lang",userv2.lang);


const userv3 = new Programmer("Varvara", 18, 5000, ["JavaScript", "Python", "Java"]);
console.log("userv3",userv3)
console.log("name",userv3.name);
console.log("age",userv3.age);
console.log("salary",userv3.salary);
console.log("lang",userv3.lang);
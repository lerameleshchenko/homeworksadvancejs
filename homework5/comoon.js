const API = "https://ajax.test-danit.com/api/json/";
const container = document.querySelector(".container");
const feed = container.querySelector(".feed");
const loader = document.querySelector(".loader")

function sendRequest(url, method = "GET", options) {
    return fetch(url, { method: method, ...options })
        .then(response => response.json());
}

class Card {
    constructor(post) {
        this.post = post;
    } 
    createElementCard(){
    
        const {name,email,title,body,id}= this.post;
        const div = document.createElement("div");
        div.classList.add("card");

        const divContent = document.createElement("div")
        divContent.classList.add("content")

        const userName = document.createElement("p");
        userName.textContent = `name: ${name}`;

        const userEmail = document.createElement("p");
        userEmail.textContent = `email: ${email}`;

        const postTitle = document.createElement("p");
        postTitle.textContent = `title: ${title}`;

        const postBody = document.createElement("p");
        postBody.classList.add("postBody")
        postBody.textContent = `text: ${body}`;

        const buttonDelete = document.createElement("button");
        buttonDelete.classList.add("cardDelete")
        buttonDelete.textContent = "DELETE"
          
        buttonDelete.addEventListener("click", ()=> {
            fetch(`${API}posts/${id}`, {
                method: "DELETE",
            })
            .then((response) => {
                if (response.status === 200) {
                    const div = document.querySelector(".card")
                    div.remove();
                }
            })
            .catch((error) => {
                console.error("Помилка при видаленні публікації", error);
            });
        })


        divContent.append(userName, userEmail, postTitle, postBody);
        div.append(divContent,buttonDelete)
        return div;
    
}

    }

function createCard() {
    sendRequest(`${API}users`)
        .then(userData => {
            sendRequest(`${API}posts`)
                .then(postData => {
                    userData.forEach(user => {
                        const userPosts = postData.filter(post => post.userId === user.id);
                        console.log(userPosts)
                        userPosts.forEach(post => {
                            const card = new Card({ ...user, ...post });
                            console.log(card)
                            feed.append(card.createElementCard());
                        });
                    });
                })
                .finally(() => {
                    loader.remove();
                });
        });
}





createCard();



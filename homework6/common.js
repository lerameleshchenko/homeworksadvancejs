// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, 
// отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати
//  інформацію про фізичну адресу.
// // під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – 
// континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

async function sendRequest(url, method, options) {
    const response = await fetch(url, { method: method, ...options })
    const result =  response.json() 
    return result
}

const buttonIP = document.querySelector(".findIP")
const info = document.querySelector(".info")
const loader = document.querySelector(".loader");
let buttonClicked = false;

buttonIP.addEventListener("click", async()=>{
    if (!buttonClicked){
        buttonClicked = true;
        const findIPk = await sendRequest("https://api.ipify.org/?format=json")
          const {ip} = findIPk

        const findIP = await sendRequest(`http://ip-api.com/json/${ip}`)
          const {country, regionName, city, timezone} = findIP //району там немає
     
      info.style.border = "1px solid black"
     info.insertAdjacentHTML("beforeend", `
     <div class="content"> 
            <p>континент: ${timezone}</p>
            <p>країна: ${country}</p>
            <p>регіон: ${regionName}</p>
            <p>місто: ${city}</p>
              </div>
     `)

    }
   
})


   



